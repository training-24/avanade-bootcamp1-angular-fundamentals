import { NgClass, NgForOf, NgIf, UpperCasePipe } from '@angular/common';
import { Component } from '@angular/core';
import { DemoTodolistComponent } from './demo-todolist.component';
import { DemoBlockForComponent } from './pages/demo-block-for.component';
import { DemoBlockIfComponent } from './pages/demo-block-if.component';
import { DemoBlockSwitchComponent } from './pages/demo-block-switch.component';
import { DemoBlockTabbarComponent } from './pages/demo-block-tabbar.component';
import { DemoCompoPhoneComponent } from './pages/demo-compo-phone.component';
import { DemoForComponent } from './pages/demo-for.component';
import { DemoIfComponent } from './pages/demo-if.component';
import { labels } from './label';
import { DemoInputComponent } from './pages/demo-input.component';
import { DemoPipesComponent } from './pages/demo-pipes.component';
import { DemoSignal1Component } from './pages/demo-signal1.component';
import { DemoSignal2Component } from './pages/demo-signal2.component';
import { DemoStylesComponent } from './pages/demo-styles.component';
import { DemoSwitchComponent } from './pages/demo-switch.component';
import { DemoTodolistWithSignalsComponent } from './pages/demo-todolist-with-signals.component';
import { DemoTooltipComponent } from './pages/demo-tooltip.component';
import { PricingComponent } from './shared/pricing.component';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    DemoStylesComponent,
    DemoTooltipComponent,
    DemoInputComponent,
    DemoPipesComponent,
    DemoIfComponent,
    DemoForComponent,
    DemoSwitchComponent,
    NgIf,
    NgForOf,
    DemoTodolistComponent,
    NgClass,
    DemoSignal1Component,
    DemoSignal2Component,
    DemoBlockIfComponent,
    DemoBlockForComponent,
    DemoBlockSwitchComponent,
    UpperCasePipe,
    DemoBlockTabbarComponent,
    DemoTodolistWithSignalsComponent,
    DemoCompoPhoneComponent
  ],
  template: `
    <div class="flex flex-wrap gap-2">
      
     <!-- <button 
        class="btn"
        *ngFor="let p of pages"
        [ngClass]="{
          'btn-info': p === page
        }"
        (click)="changePage(p)"
      >
        {{p}}
      </button>-->
      
      <select
        class="select select-bordered w-full max-w-xs"
        (change)="changePage($event)"
      >
        <option
          *ngFor="let p of pages"
          [selected]="p === page"
          [value]="p"
        > {{p | uppercase}} </option>
      </select>
    </div>
    
    <div class="max-w-screen-lg mx-6 lg:mx-auto">
      <app-demo-styles *ngIf="page === 'demo-styles'" />
      <app-demo-tooltip  *ngIf="page === 'demo-tooltip'" />
      <app-demo-input  *ngIf="page === 'demo-input'" />
      <app-demo-pipes  *ngIf="page === 'demo-pipes'" />
      <app-demo-if  *ngIf="page === 'demo-if'" />
      <app-demo-for  *ngIf="page === 'demo-for'" />
      <app-demo-switch  *ngIf="page === 'demo-switch'" />
      <app-demo-todolist  *ngIf="page === 'demo-todolist'" />
      <app-demo-signal1  *ngIf="page === 'demo-signal1'" />
      <app-demo-signal2  *ngIf="page === 'demo-signal2'" />
      <app-demo-block-if *ngIf="page === '@if'" />
      <app-demo-block-for *ngIf="page === '@for'" />
      <app-demo-block-switch *ngIf="page === '@switch'" />
      <app-demo-block-tabbar *ngIf="page === 'demo-block-tabbar'" />
      <app-demo-todolist-with-signals  *ngIf="page === 'demo-todo-signals'" />
      <app-demo-compo-phone *ngIf="page === 'demo-phone'" />
    </div>
  `,
})
export class AppComponent {
  page: string = 'demo-phone'

  pages = [
    'demo-styles', 'demo-tooltip', 'demo-input', 'demo-pipes', 'demo-if', 'demo-for', 'demo-switch', 'demo-todolist',
    'demo-signal1', 'demo-signal2',
    '@if', '@for', '@switch', 'demo-block-tabbar', 'demo-todo-signals', 'demo-phone'
  ]

  changePage(event: Event) {
    this.page = (event.target as HTMLSelectElement).value;
  }
}

