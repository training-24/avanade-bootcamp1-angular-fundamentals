import { JsonPipe, NgClass, NgForOf, NgIf } from '@angular/common';
import { Component, computed, signal } from '@angular/core';

type Todo = {
  id: number;
  title: string;
  completed: boolean
}

@Component({
  selector: 'app-demo-todolist-with-signals',
  standalone: true,
  imports: [
    NgForOf,
    NgClass,
    JsonPipe,
    NgIf
  ],
  template: `
    <h1>Todo list Signals ({{totalTodos()}})</h1>

    <!--<todo-form>-->
    <input type="text" (keydown.enter)="addTodo(input)" #input>

    <!--<messages>-->
    @if(isEmpty()) {
      <div>Non ci sono todo</div>  
    }
    
    <!--<todo-list>-->
    @for (todo of todos(); track todo.id) {
      <div
        [ngClass]="{ 'line-through': todo.completed }"
        (click)="toggleTodo(todo, $event)"
      >
        <input
          type="checkbox"
          [checked]="todo.completed"
          (click)="toggleTodo(todo, $event)"
        >
        {{todo.id}} - {{ todo.title }}

        <i class="fa fa-trash" (click)="removeTodo(todo, $event)"></i>

      </div>
    } @empty {
      <div>Non ci sono todo!!!</div>
    }

    <pre>{{todos | json}}</pre>
  `,
  styles: ``
})
export class DemoTodolistWithSignalsComponent {
  todos = signal<Todo[]>([
    { id: 11, title: 'Todo 1', completed: true },
    { id: 22, title: 'Todo 2', completed: false },
    { id: 33, title: 'Todo 3', completed: true },
  ])

  isEmpty = computed(() => !this.todos().length)
  totalTodos = computed(() => this.todos().length)

  addTodo(input: HTMLInputElement) {
    const newTodo: Todo = {
      id: Date.now(),
      title: input.value,
      completed: false
    }
    // this.todos.set([ ...this.todos(), newTodo ])
    // this.todos.update(todos => [ ...todos, newTodo ])
    this.todos.update(todos => [ ...todos, newTodo ])
  }

  removeTodo(todoToRemove: Todo, event: MouseEvent) {
    event.stopPropagation();
    this.todos.update(todos => todos.filter(t => t.id !== todoToRemove.id))
  }

  toggleTodo(todoToToggle: Todo, event: Event) {
    event.stopPropagation();
    this.todos.update(todos => todos.map(
      t => t.id === todoToToggle.id ? { ...t, completed: !t.completed} : t
    ))
  }
}
