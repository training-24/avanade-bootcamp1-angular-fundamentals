import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo-block-switch',
  standalone: true,
  imports: [],
  template: `
   <h1> Demo Switch block</h1>
   
   <div>
     @switch (currentStep()) {
       @case ('step1') {
         <h2>Step 1</h2>
         <input class="input input-bordered" type="text">
         <input class="input input-bordered" type="text">
         <input class="input input-bordered" type="text">
         <button class="btn" (click)="currentStep.set('step2')">Go to step 2</button>
       }
       @case ('step2') {
         <h2>Step 1</h2>
         <div>bla bla</div>
         <button class="btn" (click)="currentStep.set('final')">Next</button>
       }
       @case ('final') {
        <div>
          Grazie!
        </div>
       }
       @default {
         <div>Welcome</div>
         <button class="btn" (click)="currentStep.set('step1')">Start Quiz</button>
       }
     }
   </div>
  `,
  styles: ``
})
export class DemoBlockSwitchComponent {
  currentStep = signal<'step1' | 'step2' | 'final' | null>(null)

  constructor() {


  }
}
