import { CommonModule, DatePipe, DecimalPipe, JsonPipe, UpperCasePipe } from '@angular/common';
import { Component } from '@angular/core';
import { time } from '@ngtools/webpack/src/benchmark';

@Component({
  selector: 'app-demo-pipes',
  standalone: true,
  imports: [
   /* DatePipe,
    DecimalPipe,
    JsonPipe,
    UpperCasePipe*/
    CommonModule
  ],
  template: `
    <h1>Pipes</h1>
    
    <div>{{today | date: 'dd/MM-yyyy'}}</div>
    <div>
      {{timestamp | date: 'MMM'}} at 
      {{timestamp | date: 'hh:mm:ss'}}
    </div>
    
    <div>{{name | uppercase}}</div>
    <div>{{value | number: '1.2-4'}}</div>
    
    <pre>{{users | json}}</pre>
  `,
  styles: ``
})
export class DemoPipesComponent {
  name = 'fabio'
  today = new Date();
  timestamp = 1702423006;
  value = 1.212912661294612971
  user = { name: 'Fabio', surname: 'Biondi' }
  users = [
    { name: 'Fabio', surname: 'Biondi' },
    { name: 'Fabio', surname: 'Biondi' }
  ]
}
