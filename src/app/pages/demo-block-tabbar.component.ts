import { Component, signal } from '@angular/core';



type Product = {
  id: number;
  name: string;
  description: string;
  img?: string;
  cost: number;
  city: string;
}

@Component({
  selector: 'app-demo-block-tabbar',
  standalone: true,
  template: `
    <h1>Demo Tabbar with blocks</h1>

    <div class="join">
      @for (product of products(); track product.id) {
        <button
          class="btn primary join-item"
          (click)="selectProduct(product)"
        >
          {{ product.name }}
        </button>
      }
    </div>

    @if (activeProduct()) {
      <div class="alert alert-info">
        {{ activeProduct()?.description }}

        @if (activeProduct()?.img; as image) {
          <img [src]="image" [alt]="activeProduct()?.name" width="100">
        } @else {
          <div>no photo</div>
        }

        <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + activeProduct()?.city + '&zoom=7&size=800x400&key=YOUR_TOKEN'" alt="">
        
      </div>
    }
  `,
})
export class DemoBlockTabbarComponent {
  products = signal<Product[]>([
    {id: 1, city: 'Milan', name: 'Chocolate', cost: 3, description: 'lorem ...', img: 'https://laderach.com/media/catalog/product/cache/2bcef483fa0e6036aa72e289994cd384/1/0/10004783_10097037_FrischSchoggi_Herz_Haselnuss_Milch-01.jpg'},
    {id: 2, city: 'Palermo', name: 'Milk', cost: 1, description: 'bla bla..', img: 'https://i5.walmartimages.com/seo/Great-Value-Whole-Vitamin-D-Milk-Gallon-128-fl-oz_6a7b09b4-f51d-4bea-a01c-85767f1b481a.86876244397d83ce6cdedb030abe6e4a.jpeg?odnHeight=640&odnWidth=640&odnBg=FFFFFF'},
    {id: 3, city: 'New York', name: 'Biscuits', cost: 2, description: 'super good!'},
  ]);

  activeProduct = signal<Product | null>(null)

  selectProduct(product: Product) {
    this.activeProduct.set(product)
  }
}

