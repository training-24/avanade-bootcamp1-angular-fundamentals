import { JsonPipe, NgForOf, NgIf } from '@angular/common';
import { Component, computed, signal } from '@angular/core';

export interface Product {
  id: number;
  name: string;
  cost: number
}

@Component({
  selector: 'app-demo-block-for',
  standalone: true,
  imports: [
    JsonPipe,
    NgForOf,
    NgIf
  ],
  template: `
    <h1>ngFor vs for block</h1>
    
    <input
      placeholder="Name"
      class="input input-bordered"
      type="text" (keydown.enter)="addProduct(inputName, inputCost)" #inputName
    >
    <input
      placeholder="Cost"
      class="input input-bordered"
      type="number" (keydown.enter)="addProduct(inputName, inputCost)" #inputCost
    >
    <button class="btn" (click)="addProduct(inputName, inputCost)">add</button>
    
    <div>There are {{ totalProducts() }} products</div>
    
    <hr>
    <li *ngFor="let product of products(); let i = index">
      {{i}} {{product.name}}
    </li>
    
    <div *ngIf="isEmpty()">no products</div>
    
    <hr>
    
    @for(product of products(); track product.id; ) {
      <li>
        {{$index + 1}} {{product.name}}
        <i class="fa fa-trash" (click)="removeProduct(product)"></i>
        @if ($last) {
          <button>xyz</button>
        }
      </li>
    } @empty {
      no products
    }
    
    
    <pre>{{products() | json}}</pre>
  `,
  styles: ``
})
export class DemoBlockForComponent {
  products = signal<Product[]>([
    { id: 1, name: 'Nutella', cost: 5 },
    { id: 2, name: 'Pane', cost: 1 },
    { id: 3, name: 'Latte', cost: 2 },
  ])
  isEmpty = computed(() => this.products().length === 0)
  totalProducts = computed(() => this.products().length)

  removeProduct(productToRemove: Product) {
    /*this.products.set(
      this.products().filter(p => p.id !== productToRemove.id)
    )*/

    this.products.update(products => products.filter(p => p.id !== productToRemove.id))
  }

  addProduct(inputName: HTMLInputElement, inputCost: HTMLInputElement) {
    const newProduct: Product = {
      id: Date.now(),
      name: inputName.value,
      cost: +inputCost.value
    }
    this.products.update(products => [...products, newProduct])

    inputName.value = '';
    inputCost.value = '';
  }
}
