import { NgClass, NgIf, NgSwitch, NgSwitchCase, NgSwitchDefault } from '@angular/common';
import { Component } from '@angular/core';

export type Section = 'home' | 'step1' | 'step2' | 'final'

@Component({
  selector: 'app-demo-switch',
  standalone: true,
  imports: [
    NgIf,
    NgSwitch,
    NgSwitchCase,
    NgSwitchDefault,
    NgClass
  ],
  template: `
    <h1>ngSwitch</h1>
    
    <div class="flex gap-6 justify-center w-full">
      <button  (click)="changeSection('home')">Home</button>
      <button  (click)="changeSection('step1') ">Step1</button>
      <button  (click)="changeSection('step2') ">Step2</button>
      <button  (click)="changeSection('final') ">Final</button>
     
    </div>
    
    <div [ngSwitch]="section">
      <div *ngSwitchCase="'home'">home</div>
      <div *ngSwitchCase="'step1'">step1 homepage</div>
      <div *ngSwitchCase="'step2'">step2 homepage</div>
      <div *ngSwitchCase="'final'">final</div>
      <div *ngSwitchDefault>Select  button</div>
    </div>
    
  `,
  styles: ``
})
export class DemoSwitchComponent {
  section: Section | null = null;

  changeSection(section: Section) {
    this.section = section;
  }

}


