import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-tooltip',
  standalone: true,
  imports: [],
  template: `
    <h1>Demo Tooltip</h1>

    
    <button
      class="btn primary"
      (mousemove)="show($event)"
      (mouseout)="hide()"
    > OVER
    </button>
    
    <input type="text" (blur)="blurHandler($event)">

    <div
      class="absolute  bg-black text-white p-3 rounded-xl pointer-events-none"
      [style.left.px]="coords?.x"
      [style.top.px]="coords?.y"
      [hidden]="!coords"
    >
      TOOLTIP
    </div>
    

  `,
  styles: ``
})
export class DemoTooltipComponent {
  coords: Coords | null = null;

  show(event: MouseEvent) {
    this.coords = {
      x: event.clientX,
      y: event.clientY,
    }
    console.log('rollover', event.clientX, event.clientY)
  }

  hide() {
    this.coords = null;
  }

  blurHandler(event: FocusEvent) {
    console.log(event.target)
  }
}

interface Coords {
  x: number;
  y: number;
}
