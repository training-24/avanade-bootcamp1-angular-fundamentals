import { NgIf } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-if',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
    <h1>ngIf</h1>
    
    <div>
      <button (click)="visible = !visible">TOGGLE</button>
      {{visible}}
      <img src="assets/angular.png" alt="" *ngIf="visible">
    </div>
  `,
  styles: ``
})
export class DemoIfComponent {
  visible = true;

}
