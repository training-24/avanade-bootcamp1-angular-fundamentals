import { CommonModule, NgClass, NgStyle } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-styles',
  standalone: true,
  imports: [
    NgClass,
    NgStyle,
    // CommonModule
  ],
  template: `
    <h1>Demo Styles</h1>
    
    <div style="background-color: {{color}}">hello 1</div>

    <div
      [style.background-color]="color"
      [style.padding.px]="padding"
    >hello 2</div>

    <div [ngStyle]="css">
      Hello 3
    </div>

    <div
      class="alert"
      [ngClass]="{
        'primary': alertType === 'primary',
        'success': alertType === 'success',
        'error': alertType === 'error',
      }"
    > {{ alertType }} </div>

    <button
      (click)="alertType = 'primary'"
    >primary</button>

    <button (click)="alertType = 'success'">success</button>
    <button (click)="alertType = 'error'">error</button>

  `,
  styles: `
    .alert {
      padding: 10px;
      border: 1px solid black;
      border-radius: 20px;
    }

    .success {
      background-color: green;
    }

    .error {
      background-color: red;
    }

    .primary {
      background-color: blue;
    }
  `

})
export class DemoStylesComponent {
  alertType = 'success'
  css: CustomCSS = { fontSize: '30px'};

  color = 'orange'
  padding = 10

  constructor() {
    setTimeout(() => {
      this.css = { fontSize: '50px', padding: '10px'}
    }, 2000)
  }
}

interface CustomCSS {
  fontSize: string;
  padding?: string  ;
}
