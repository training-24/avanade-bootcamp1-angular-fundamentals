import { NgIf } from '@angular/common';
import { Component, computed, effect, signal, untracked } from '@angular/core';

@Component({
  selector: 'app-demo-signal1',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
    <h1
      [style.color]="getColor()"
    >Demo Signal {{counter()}}</h1>
    
    <button class="btn btn-info" (click)="inc()">+</button>
    <button class="btn btn-info" (click)="dec()">-</button>
    <button class="btn" (click)="reset()" *ngIf="!isZero()">Reset</button>
    
    <button (click)="doNOthing()">do nothing</button>
  `,
  styles: ``
})
export class DemoSignal1Component {
  counter = signal(0)
  isZero = computed(() => this.counter() === 0)
  getColor = computed(() => this.isZero() ? 'red' : 'green')

  constructor() {
    effect(() => {
      localStorage.setItem('counter', this.counter().toString())
    });

    effect(() => {
      console.log('color', this.getColor(), untracked(this.counter))
    });
  }

  dec() {
    this.counter.update(c => c - 1)
  }
  inc() {
    this.counter.update(c => c + 1)
  }

  reset() {
    this.counter.set(0)
  }

  doNOthing() {

    console.log('do nothing')
  }


}
