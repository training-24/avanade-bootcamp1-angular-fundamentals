import { DecimalPipe, JsonPipe, NgClass, NgForOf, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-demo-for',
  standalone: true,
  imports: [
    JsonPipe,
    NgForOf,
    DecimalPipe,
    NgIf,
    NgClass
  ],
  template: `
    <h1>ngFor</h1>
    
    <li 
      *ngFor="let product of products; let i = index; let last = last; let first = first; let odd = odd"
      [ngClass]="{
        'border-b-2 border-white': last,
        'bg-sky-300': odd
      }"
    >
     {{i + 1}}. {{ product.name }} - {{odd}}
      
      <button *ngIf="last">CLICK ME</button>
    </li>
    
    <pre>{{products | json}}</pre>
  `,
  styles: ``
})
export class DemoForComponent {
  products: Product[] = [
    {id: 1, name: 'Chocolate', cost: 3},
    {id: 2, name: 'Milk', cost: 1},
    {id: 3, name: 'Biscuits', cost: 2},
    {id: 3, name: 'Biscuits', cost: 2},
    {id: 3, name: 'Biscuits', cost: 2},
  ]

  constructor() {
    for(let product of this.products) {
      console.log(product.name)
    }
  }
}
