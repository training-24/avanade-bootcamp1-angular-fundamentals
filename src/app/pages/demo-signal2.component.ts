import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-signal2',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo-signal2 works!
    </p>
  `,
  styles: ``
})
export class DemoSignal2Component {

}
