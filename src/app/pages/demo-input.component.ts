import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

export const urlRegex = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;

@Component({
  selector: 'app-demo-input',
  standalone: true,
  imports: [],
  template: `
    
    <h1>Demo Input / After</h1>
    <input
      type="text" class="input input-bordered"
      placeholder="Write Url"
      (keydown.enter)="doSomething($event)"
      #inputUrl
    >
    
    <button class="btn btn-primary" (click)="giveFocus(inputUrl)">GIVE FOCUS</button>
    
  `,
})
export class DemoInputComponent implements AfterViewInit {
  @ViewChild('inputUrl') myInput: ElementRef<HTMLInputElement> | undefined;

  ngAfterViewInit() {
    this.myInput?.nativeElement.focus()
  }

  doSomething(e: Event) {
    const text = (e.target as HTMLInputElement).value;
    const isUrlValid = urlRegex.test(text)

    if(isUrlValid ) {
      window.open(text)
    }
  }

  giveFocus(input: HTMLInputElement) {
    console.log(input.value)
  }
}

