import { Component, signal } from '@angular/core';
import { PhoneComponent } from '../shared/phone.component';

@Component({
  selector: 'app-demo-compo-phone',
  standalone: true,
  imports: [
    PhoneComponent
  ],
  template: `
    <h1>Phone Component Demo</h1>
    
    <app-phone
      image="https://images.unsplash.com/photo-1544376798-89aa6b82c6cd?q=80&w=300&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
      title="barca"
      showTitle
      size="sm"
    ></app-phone>


    <app-phone
      [image]="url()"
      [title]="msg"
      [showTitle]="true"
      size="lg"
    ></app-phone>
    
    
    <button (click)="doNothing()">click</button>

  `,
  styles: ``
})
export class DemoCompoPhoneComponent {
  url = signal('https://images.unsplash.com/photo-1599409522056-e7fab1dadfec?q=80&w=300&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')
  msg  = 'landscape'

  doNothing() {

  }
}
