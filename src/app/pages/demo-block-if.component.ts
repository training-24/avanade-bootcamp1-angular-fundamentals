import { NgIf } from '@angular/common';
import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo-block-if',
  standalone: true,
  template: `
    <h1>if block</h1>
    
    <h3>logged {{logged()}}</h3>

    @if (logged()) {
      <div>sono loggato</div>
      <button class="btn" (click)="logout()">SignOut</button>
    } @else {
      <button class="btn" (click)="signIn()">SignIn</button>
    }
  `,
})
export class DemoBlockIfComponent {

  logged = signal(false)

  signIn() {
    this.logged.set(true)
  }

  logout() {
    this.logged.set(false)
  }
}
