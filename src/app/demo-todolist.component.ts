import { JsonPipe, NgClass, NgForOf, NgIf } from '@angular/common';
import { Component } from '@angular/core';

type Todo = {
  id: number;
  title: string;
  completed: boolean
}

@Component({
  selector: 'app-demo-todolist',
  standalone: true,
  imports: [
    NgForOf,
    NgClass,
    JsonPipe,
    NgIf
  ],
  template: `
    <h1>Todo list ({{todos.length}})</h1>
    <h3>Mutability vs Immutability</h3>
    
    <input type="text" (keydown.enter)="addTodo(input)" #input>
    
    <div *ngIf="!todos.length">Non ci sono todo</div>
    
    <div
      *ngFor="let todo of todos; let i = index;"
      [ngClass]="{ 'line-through': todo.completed }"
      (click)="toggleTodo(todo, $event)"
    >
      <input 
        type="checkbox" 
        [checked]="todo.completed"
        (click)="toggleTodo(todo, $event)"
      >
      {{todo.id}} - {{ todo.title }}
      
      <i class="fa fa-trash" (click)="removeTodo(todo, $event)"></i>
      
    </div>
    
    <pre>{{todos | json}}</pre>
    
  `,
  styles: ``
})
export class DemoTodolistComponent {
  todos: Todo[] = [
    { id: 11, title: 'Todo 1', completed: true },
    { id: 22, title: 'Todo 2', completed: false },
    { id: 33, title: 'Todo 3', completed: true },
  ]

  addTodo(input: HTMLInputElement) {
    const newTodo: Todo = {
      id: Date.now(),
      title: input.value,
      completed: false
    }
    // this.todos = this.todos.concat([newTodo])
    this.todos = [ ...this.todos, newTodo ]
    /*
    this.todos.push(newTodo)
    */
  }

  removeTodo(todoToRemove: Todo, event: MouseEvent) {
    event.stopPropagation();
    this.todos = this.todos.filter(t => t.id !== todoToRemove.id)
    /*
    const index = this.todos.findIndex(t => t.id === todoToRemove.id)
    this.todos.splice(index, 1)
    */
  }

  toggleTodo(todoToToggle: Todo, event: Event) {
    event.stopPropagation();
    this.todos = this.todos.map(
      t => t.id === todoToToggle.id ? { ...t, completed: !t.completed} : t
    )
    /*
    const index = this.todos.findIndex(t => t.id === todoToToggle.id)
    this.todos[index].completed = !this.todos[index].completed
    */
  }
}
