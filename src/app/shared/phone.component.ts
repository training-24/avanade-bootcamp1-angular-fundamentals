import { UpperCasePipe } from '@angular/common';
import { booleanAttribute, Component, Input, numberAttribute } from '@angular/core';

@Component({
  selector: 'app-phone',
  standalone: true,
  imports: [
  ],
  template: `
    <div class="mockup-phone">
      <div class="camera"></div>
      <div title="" class="display">
        <div class="artboard artboard-demo phone-1">
          @if (showTitle) {
            {{ title }}
          }
          
          @if(img) {
            <img
              [src]="img"
              [alt]="title"
              [style.width.%]="size"
            >  
          } @else {
            Ricordati di impostare una foto!
          }
        </div>
      </div>
    </div>
  `,
})
export class PhoneComponent {
  @Input({ alias: 'image'}) img: string | undefined;

  @Input({
    transform: booleanAttribute
  }) showTitle: boolean = false

  @Input({
    required: true,
    transform: (val: string) => {
      return val.toUpperCase()
    }
  }) title = ''

  @Input({
    transform: (size: 'sm' | 'lg' | 'xl'  ): number => {
      switch (size) {
        case 'sm': return 50;
        case 'lg': return 75;
        case 'xl': return 100;
      }
    }
  })
  size: number = 100

  ngOnInit() {
    console.log(this.title)
  }

}
